FROM registry.gitlab.com/learnopentech/np:bookworm
RUN DEBIAN_FRONTEND=noninteractive apt-get update; apt-get install -y \
    composer \
    php-apcu \
    php-curl \
    php-dompdf \
    php-bcmath \
    php-gd \
    php-mysql \
    php-simplexml \
    php-uploadprogress \
    php-zip && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN composer create-project opigno/opigno-composer /var/www/opigno --ignore-platform-req=ext-dom --no-install
RUN composer require --working-dir=/var/www/opigno 'drupal/commerce_stripe:^1.0' 'drupal/smtp:^1.2'
RUN composer install --working-dir=/var/www/opigno
RUN mkdir -p /var/www/opigno/web/sites/default/files && chown www-data:www-data /var/www/opigno/web/sites/default/files
VOLUME /var/www/opigno/web/sites/default/files
RUN echo 'post_max_size = 100M\nupload_max_filesize = 100M' > /etc/php/8.2/fpm/conf.d/50-post-max-size.ini
ADD opigno-nginx.conf /etc/nginx/sites-available/opigno
WORKDIR /var/www/opigno
ADD opigno-settings.php web/sites/default/settings.php
RUN chown -R www-data:www-data /var/www/opigno/web/sites && \
    mkdir -p /srv/opigno/private && chown -R www-data:www-data /srv/opigno/private
RUN ln -s /etc/nginx/sites-available/opigno /etc/nginx/sites-enabled && rm /etc/nginx/sites-enabled/default
RUN mkdir -p /srv/opigno/private && chown www-data:www-data /srv/opigno/private && \
    mkdir -p /srv/opigno/config/sync && chown -R www-data:www-data /srv/opigno/config
VOLUME /srv/opigno/private
VOLUME /srv/opigno/config
